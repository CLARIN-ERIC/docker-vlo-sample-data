#!/bin/bash
export VLO_VERSION=4.3.0-beta2
export SOLR_IMAGE="registry.gitlab.com/clarin-eric/docker-solr:1.0.0-beta3"
export VLO_IMAGE="registry.gitlab.com/clarin-eric/docker-vlo-beta:1.3.0-beta3"
    
init_data (){
	export SAMPLE_DATA_FILE="${PWD}/sample-data.tar.gz"
	export BUILD_DIR="${PWD}/build" 

	if [ -d ${BUILD_DIR} ]; then
		cleanup_data
	fi
	
	mkdir -p ${BUILD_DIR}

	install_dependencies $@	
	prepare_data $@
	create_solr_data $@
}

prepare_data() {
	#####
	# Copy vlo config to build dir
	####
	cp -r vlo-config ${BUILD_DIR}/vlo-config
	
	#####
	# Extract sample data to build dir
	####
	echo "Extracting sample data"
	(mkdir ${BUILD_DIR}/sample-data && cd ${BUILD_DIR}/sample-data && \
		tar zxf "$SAMPLE_DATA_FILE")
		
	#####
	# Extract VLO sources to build dir (we need the Solr home)
	# [TODO]: Get solr config out of specified VLO image instead
	####
	echo "Getting VLO"
	(mkdir ${BUILD_DIR}/vlo && cd ${BUILD_DIR}/vlo && \
		curl -s -L "https://github.com/clarin-eric/VLO/archive/vlo-${VLO_VERSION}.tar.gz"| tar zxf - --strip-components=1 )
}

create_solr_data() {
	#####
	# Start Solr and VLO containers (see docker-compose.yml)
	####	
	docker-compose up -d
	
	# Wait until Tomcat is available in the VLO container, which means all necessary init has taken place    
    echo -n "Waiting until started..."
    while ! docker-compose exec -T vlo_sample_build_importer curl http://localhost:8080 > /dev/null 2>&1 ; do
    	sleep 1
    	echo -n "."
    done
    echo " started!"
    
	#####
	# Start a VLO import
	####	    
    echo "Starting import..."
    docker logs -f --since 0s image_vlo_sample_build_importer_1 &
    docker-compose exec -T vlo_sample_build_importer bash -c "cd /opt/vlo/bin && ./vlo_solr_importer.sh"
    
    # Bring containers down, to make sure that Solr is shut down properly
    docker-compose down
        
	#####
	# Extract the Solr data content
	####	  
    echo "Copying over solr data..."
    docker-compose run -d --entrypoint "bash -c 'while true; do sleep 1; done'" vlo_sample_build_solr
    docker cp $(docker-compose ps -q):/solr-data "${BUILD_DIR}/solr-data"
        
	#####
	# Solr data content check
	####	  
    if [ ! -d "${BUILD_DIR}/solr-data/vlo-index" ]; then
    	echo "Solr data directory was not created successfully"
    	find "${BUILD_DIR}/solr-data"
    	docker-compose logs
    	sleep 10 #leave some time for output to flush to Gitlab CI
    	exit 1
    fi

	#####
	# Clean up after containers
	####	    
    docker-compose down -v
}

cleanup_data () {
    if [ -d ${BUILD_DIR} ]; then
      echo "Removing existing build directory"
      rm -rf "${BUILD_DIR}";
    fi
}

install_dependencies() {
	if [ "$1" != "local" ]
	then
		if ! which apk; then
			echo "apk not found - may not be able to run docker compose"
		else
			apk --quiet update --update-cache
			apk --quiet add 'curl'
			apk --quiet add 'py2-pip=9.0.0-r1'
			pip --quiet --disable-pip-version-check install 'docker-compose==1.16.1'
		fi
	fi
}